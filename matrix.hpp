#pragma once
#include <iostream>


namespace mt{

    //#define MY_DEBUG

    template<typename T, int N, int M>
    struct MasWrapper
    {
        T mas[N][M];
    };

    template<typename T, int N, int M>
    class Matrix{
    public:

        Matrix(){
#ifdef MY_DEBUG
            std::cout << "Constructor" << std::endl;
#endif
            m_n = N;
            m_m = M;
            for (int i = 0; i < m_n; i++)
                for (int j = 0; j < m_m; j++)
                    m_mat[i][j] = 0;
        }


        Matrix(const T mas[N][M]){
#ifdef MY_DEBUG
            std::cout << "Constructor" << std::endl;
#endif
            m_n = N;
            m_m = M;
            for (int i = 0; i < m_n; i++)
                for (int j = 0; j < m_m; j++)
                    m_mat[i][j] = mas[i][j];
        }

        Matrix(const MasWrapper<T, N, M>& mas){

#ifdef MY_DEBUG

            std::cout << "Constructor" << std::endl;

#endif
            m_n = N;
            m_m = M;
            for (int i = 0; i < m_n; i++)
                for (int j = 0; j < m_m; j++)
                    m_mat[i][j] = mas.mas[i][j];
        }

        Matrix(const Matrix<T, N, M>& mat)
        {
#ifdef MY_DEBUG
            std::cout << "Copy constructor" << std::endl;
#endif

            m_n = mat.m_n;
            m_m = mat.m_m;

            for (int i = 0; i < m_n; i++)
                for (int j = 0; j < m_m; j++)
                    m_mat[i][j] = mat.m_mat[i][j];
        }

        int getN() const { return m_n; }
        int getM() const { return m_m; }
        int get(int i, int j) const { return m_mat[i][j]; }
        void set(int i, int j, T data) { m_mat[i][j] = data; }




        template<typename U, int B, int V>

        Matrix<T, N, M>& operator=(const Matrix<T, N, M>& mat){
#ifdef MY_DEBUG
            std::cout << "Operator =" << std::endl;
#endif
            m_n = mat.getN();
            m_m = mat.getM();

            for(int i = 0; i < m_n; i++)
                for(int j = 0; j < m_m ; j++)
                    m_mat[i][j] = mat.get(i,j);
            return *this;
        }


        T determinant(){
#ifdef MY_DEBUG
            std::cout << "Определитель: " << std::endl;
#endif

            T det = 0;
            if( m_n == 2 && m_m == 2){
                det = m_mat[0][0] * m_mat[1][1] - m_mat[0][1] * m_mat[1][0];
                return det;
            }

            else if( m_n == 3 && m_m == 3){
                det = m_mat[0][0] * (m_mat[1][1] * m_mat[2][2] - m_mat[1][2] * m_mat[2][1]) - m_mat[0][1] * (m_mat[1][0] * m_mat[2][2] - m_mat[1][2] * m_mat[2][0]) + m_mat[0][2] * (m_mat[1][0] * m_mat[2][1] - m_mat[1][1] * m_mat[2][0]);
                return det;
            }
            else{
                std::cout<<"Операция не поддерживается"<<std::endl;
                return -1;
            }
        }


        Matrix<T, N, M> obratmatrix() {
            Matrix<T, N, M> tmp;
            if ((m_n == 2 && m_m == 2) || (m_n == 3 && m_m == 3)) {
                T det = determinant();
                if (det == 0) {
                    std::cout << "Определитель равен нулю" << std::endl;
                    return tmp;
                }

                if (m_n == 2) {
                    tmp.m_mat[0][0] = m_mat[1][1] / det;
                    tmp.m_mat[0][1] = -m_mat[0][1] / det;
                    tmp.m_mat[1][0] = -m_mat[1][0] / det;
                    tmp.m_mat[1][1] = m_mat[0][0] / det;
                    return tmp;
                }

                if (m_n == 3) {
                    tmp.m_mat[0][0] = (m_mat[1][1] * m_mat[2][2] - m_mat[2][1] * m_mat[1][2]) / det;
                    tmp.m_mat[1][0] = (m_mat[1][0] * m_mat[2][2] - m_mat[2][0] * m_mat[1][2]) / det;
                    tmp.m_mat[2][0] = (m_mat[1][0] * m_mat[2][1] - m_mat[2][0] * m_mat[1][1]) / det;
                    tmp.m_mat[0][1] = (m_mat[0][1] * m_mat[2][2] - m_mat[2][1] * m_mat[0][2]) / det;
                    tmp.m_mat[1][1] = (m_mat[0][0] * m_mat[2][2] - m_mat[2][0] * m_mat[0][2]) / det;
                    tmp.m_mat[2][1] = (m_mat[0][0] * m_mat[2][1] - m_mat[2][0] * m_mat[0][1]) / det;
                    tmp.m_mat[0][2] = (m_mat[0][1] * m_mat[1][2] - m_mat[1][1] * m_mat[0][2]) / det;
                    tmp.m_mat[1][2] = (m_mat[0][0] * m_mat[1][2] - m_mat[1][0] * m_mat[0][2]) / det;
                    tmp.m_mat[2][2] = (m_mat[0][0] * m_mat[1][1] - m_mat[1][0] * m_mat[0][1]) / det;
                    return tmp;
                }
            }
            else {
                std::cout << "Обратная матрица: Операция не поддерживается" << std::endl;
            }
        }



        Matrix<T, N, M> trans(){
#ifdef MY_DEBUG
            std::cout<<"Транспонированная матрица:"<<std::endl;
#endif

            Matrix<T, N, M> tmp;

            for (int i = 0; i < m_n; i++){
                for (int j = 0; j < m_m; j++){
                    tmp.m_mat[i][j] = m_mat[j][i];
                }
            }
            return tmp;
        }


        ~Matrix() {
#ifdef MY_DEBUG
            std::cout << "Destructor" << std::endl;
#endif
        }


        template<typename U, int B, int V>
        friend std::istream& operator>>(std::istream& in,  Matrix<T, N, M>& mat);
        template<typename U, int B, int V>
        friend std::ostream& operator<<(std::ostream& out,const Matrix<T, N, M>& mat);



    private:
        T m_mat[N][M];
        int m_n, m_m;

    };


    template<typename T, int N, int M>
    std::istream& operator>>(std::istream& in, Matrix<T, N, M>& mat){
        for(int i = 0; i < mat.m_n; i++)
            for(int j = 0; j < mat.m_m; j++)
                in >> mat.m_mat[i][j];
        return in;
    }

    template<typename T, int N, int M>
    std::ostream& operator<<(std::ostream& out, const Matrix<T, N, M>& mat){
        for(int i = 0; i < mat.m_n; i++){
            for(int j = 0; j < mat.m_m; j++)
                out << mat.m_mat[i][j] << " ";
            out << std::endl;
        }
        return out;
    }

    using Mat22d = Matrix<double, 2, 2>;



}

