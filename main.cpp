#include <iostream>
#include "matrix.hpp"
#include <cassert>


using mt::Mat22d;

int main() {

    std::cout << "=== Test 1 ===" << std::endl;{

        Mat22d A({ {
                           {1,2},
                           {3,4}
        } });

        auto B = A.trans();
        assert(B.get(0, 0) == 1);
        assert(B.get(0, 1) == 3);
        assert(B.get(1, 0) == 2);
        assert(B.get(1, 1) == 4);
    }

    std::cout << "Done!" << std::endl;

    std::cout << "=== Test 2 ===" << std::endl;{
        Mat22d A({ {
                           {10,4},
                           {4,5}
        } });
        auto det = A.determinant();
        assert(det == 34);
    }

    std::cout << "Done!" << std::endl;

    std::cout << "=== Test 3 ===" << std::endl;{
        Mat22d A({ {
                           {5,2},
                           {17,7}
        } });
        auto B = A.obratmatrix();
        assert(B.get(0, 0) == 7);
        assert(B.get(0, 1) == -2);
        assert(B.get(1, 0) == -17);
        assert(B.get(1, 1) == 5);
    }
    std::cout << "Done!" << std::endl;




    return 0;
}


